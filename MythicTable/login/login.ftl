<#import "template.ftl" as layout>
<@layout.registrationLayout doubleColumn=true; section>
    <#if section = "header">
        ${msg('doLogIn')}
    <#elseif section = "form">
        <div class="login">
            <#-- App-initiated actions should not see warning messages about the need to complete the action -->
            <#-- during login.                                                                               -->
            <#if message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                <div class="alert alert-${message.type}">
                    <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                    <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                    <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                    <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                    <span class="feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                </div>
            </#if>

            <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
                <input type="hidden" name="firstName" value="Mythic" />
                <input type="hidden" name="lastName" value="Player" />
                <div>
                    <label for="email"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>
                    <#if usernameEditDisabled??>
                        <input tabindex="1" id="username" name="username" value="${(login.username!'')}" type="text" disabled />
                    <#else>
                        <input tabindex="1" id="username" name="username" value="${(login.username!'')}"  type="text" autofocus autocomplete="off" />
                    </#if>
                </div>

                <div>
                    <label for="password">${msg("password")}</label>
                    <input tabindex="2" id="password" name="password" type="password" autocomplete="off" />
                </div>

                <div class="login-actions">
                    <#if realm.rememberMe && !usernameEditDisabled??>
                    <label class="checkbox path">
                        <input type="checkbox">
                        <svg viewBox="0 0 21 21">
                            <path d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186"></path>
                        </svg>
                    </label>
                    <div>Remember Me</div>
                    </#if>
                    <div class="spacer"></div>
                    <#if realm.resetPasswordAllowed>
                        <div class="forgot-password"><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></div>
                    </#if>
                </div>

                <div id="form-buttons">
                    <input type="hidden" id="id-hidden-input" name="credentialId" <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>
                    <button tabindex="4" name="login" id="kc-login" type="submit">${msg("doLogIn")}</button>
                </div>
            </form>
        </div>

        <div class="register">
            <div>
                <h1 class="no-margin">${msg("noAccount")}</h1>
                <button tabindex="5" onclick="window.location.href='${url.registrationUrl}'">${msg('doRegister')}</button>
            </div>
        </div>

        <div class="guest">
            <div>
                <h1 class="no-margin">${msg("guestHeader")}</h1>
                <button tabindex="6" onclick="window.location.href='${url.registrationUrl}&guest=true'">${msg('doContinueAsGuest')}</button>
            </div>
        </div>
    </#if>
</@layout.registrationLayout>