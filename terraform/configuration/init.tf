terraform {
  backend "http" {}
  required_providers {
    keycloak = {
      source  = "mrparkers/keycloak"
      version = "4.0.1"
    }
  }
}

variable "username" {
  type        = string
  default     = "admin"
  description = "An administrative username to log into the KeyCloak instance"
}

variable "password" {
  type        = string
  sensitive   = true
  default     = "admin"
  description = "The password to log into the provided administrative account on the KeyCloak instance"
}

variable "smtp_user" {
  type = string
  sensitive = true
  description = "The user to login as noreply@mythictable.com"
}

variable "smtp_password" {
  type = string
  sensitive = true
  description = "The password to smtp_user to login as noreply@mythictable.com"
}

variable "url" {
  type        = string
  default     = "http://localhost:5002"
  description = "The url to the KeyCloak instance"
}

variable "state_name" {
  type        = string
  default     = "main"
  description = "The name of the state, this should be set in CI only"
}

provider "keycloak" {
  client_id = "admin-cli"
  username  = var.username
  password  = var.password
  url       = var.url
  base_path = "/auth"
}
